﻿<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="<?= $this->get('site_subpath') ?>/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= $this->get('site_subpath') ?>/assets/social.css">

    <script src="<?= $this->get('site_subpath') ?>/assets/js/jquery-2.1.1.min.js"></script>
    <script src="<?= $this->get('site_subpath') ?>/assets/js/main.js"></script>
    <script src="<?= $this->get('site_subpath') ?>/assets/js/magnific-popup/jquery.magnific-popup.js"></script>

    <title>Dreamlist</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

</head>
<body>
<!-- Navigation -->
<?php
if ($this->isLogged()) {
    ?>
    <div class="col-lg-10 col-lg-offset-1">
        <a href="<?= $this->generate_path('main', 'index') ?>">
            <button class="btn btn-group navigation">Мои мечты</button>
        </a>
        <a href="<?= $this->generate_path('main', 'done') ?>">
            <button class="btn btn-group navigation"> Исполненные</button>
        </a>
        <a href="<?= $this->generate_path('main', 'add') ?>">
            <button class="btn btn-group btn-success navigation"> Добавить мечту</button>
        </a>
        <a>
            <button class="btn btn-group navigation"> В разработке</button>
        </a>
        <a href="<?= $this->generate_path('security', 'exit') ?>">
            <button class="btn btn-group navigation">Выход</button>
        </a>
    </div>
<?php } ?>

<!-- Main block -->
<div id="main-box">