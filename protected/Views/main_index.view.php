<?if ($this->isLogged()) {
    ?>

    <div class="col-lg-10 col-lg-offset-1">

        <?php
        // Output form notices
        if (count($this->getViewData('dreams'))) {
            echo '<h2>Невыполненные мечты:</h2>';
            foreach ($this->getViewData('dreams') as $i => $dream) {
                echo '

                        <div class="col-lg-offset-1 col-lg-10 dream" style="background: '.$dream->getImage().'">
                            <div class="col-lg-10">
                                <name>'.$dream->getName().'</name><br>
                                <description>'.$dream->getDescription().'</description>
                            </div>
                            <form action="'.$this->generate_path('main', 'realised').'" method="POST" class=" col-lg-2 realised">
                                <input name="id" type="hidden" value="'. $dream->getId().'">
                                <input name="gen" type="hidden" value="this_must_be_generated">
                                <input type="submit" value="V">
                            </form>
                        </div>
';
            }
            echo '<br>';
        } else echo '<h2>У вас нет желаний!</h2>';
        
        ?>
    </div>

<?
} else {
    $this->show_view_limited("security", "auth");
}