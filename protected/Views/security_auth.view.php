<?php

// Output form notices
if (count($this->getViewData('notices'))) {
    echo '<ul>';
    foreach ($this->getViewData('notices') as $notice) {
        echo '<li>' . $notice . '</li>';
    }
    echo '</ul><br>';
}

// Auth form
?>
<div id="login" class="login-form col-lg-offset-4 col-lg-4">
    <h1 class="register">Вход на сайт!</h1>

    <form action="<?= $this->generate_path('security', 'auth') ?>" method="POST">
        <input class="auth form-control register" type="email" name="email" id="email" placeholder="itis@example.com"
               value="itis@example.com"
               required<?= (isset($_POST['email']) ? ' value="' . $_POST['email'] . '"' : '') ?>>
        <br>
        <input class="auth form-control register" type="password" name="password" placeholder="Пароль" value="lamp" id="password"
               required>
        <br>
        <input class="btn btn-primary col-lg-4 col-lg-offset-4" type="submit" name="auth" value="Войти">
        <a class="col-lg-4 col-lg-offset-4 register" href="<?= $this->generate_path('security', 'register')?>">Регистрация</a>
    </form>
</div>	