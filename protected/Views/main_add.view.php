<?php

if (count($this->getViewData('notices'))) {
    echo '<ul>';
    foreach ($this->getViewData('notices') as $notice) {
        echo '<li>' . $notice . '</li>';
    }
    echo '</ul><br>';
}
?>

<form action="<?= $this->generate_path("main", "save") ?>" method="POST">
    <div class="col-lg-4 col-lg-offset-4 new_dream">
    <label>
        Название
        <input name="name" class="form-control" required type="text"/>
    </label><br>
    <label>
        Описание
        <textarea name="description" class="form-control" required type="text"></textarea>
    </label><br>
    <input class="btn btn-primary" type="submit">
    </div>
</form>
