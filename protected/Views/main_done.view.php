<div class="col-lg-10 col-lg-offset-1">
    <?php
    // Output form notices
    if (count($this->getViewData('dreams'))) {
        echo '<h2>Выполненные мечты:</h2><ul>';
        foreach ($this->getViewData('dreams') as $i => $dream) {
            echo '<li>' . ($i + 1) . '. ' . $dream->getName() . '<br>' .
                $dream->getDescription();
            '</li><br>';
        }
        echo '</ul><br>';
    }
    else echo '<h2>У вас нет выполненных желаний!</h2>';

    // Auth form
    ?>
</div>
