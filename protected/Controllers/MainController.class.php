<?php

//namespace MvcOop;

/**
 *  Main controller
 */
class MainController extends Controller
{

    public function indexAction()
    {
        $a = get_dreams($this->core->getUserData('id'));
        return array(
            'dreams' => $a
        );
    }

    public function doneAction()
    {
        $a = get_dreams($this->core->getUserData('id'), true);
        return array(
            'dreams' => $a
        );
    }

    public function addAction()
    {
        return array();
    }

    public function saveAction() {
        var_dump($_POST);
	$dream = $this->core->getUserData('id').'#@'.
		$_POST['name'].'#@'.
		$_POST['description'].'#@'.'0';
	add_dream($dream);
        $this->core->redirect('main', 'index', NULL);
    }

}
