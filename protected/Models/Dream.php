<?php
namespace MvcOop;

class Dream extends Model
{
    private $user;
    private $name;
    private $description;
    private $realised;

    function __construct($row, $id)
    {
        parent::__construct($id);
        $row = trim($row);
        $data = explode('#@', $row);
        $this->setUser(get_user_by_id((int)$data[0]));
        $this->setName($data[1]);
        $this->setDescription($data[2]);
        $this->setRealised($data[3]);
    }


    public function encrypt()
    {
        return $this->user->getId() . '#@'
        . $this->name . '#@'
        . $this->description . '#@'
        . $this->realised ? 1 : 0 . '#@\n';
    }

    /**
     * @param mixed $descr
     */
    public function setDescription($descr)
    {
        $this->description = $descr;
    }

public function getImage()
{
    return 'http://parachutist.ru/images/reasons_parachute_1.jpg';
}

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param boolean $realised
     */
    public function setRealised($realised)
    {
        $this->realised = $realised;
    }

    /**
     * @return boolean
     */
    public function isRealised()
    {
        return $this->realised;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

}