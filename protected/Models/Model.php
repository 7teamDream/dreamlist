<?php
/**
 * Created by IntelliJ IDEA.
 * User: zak
 * Date: 28.11.14
 * Time: 0:07
 */

namespace MvcOop;


class Model
{
    private $id;

    function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


} 